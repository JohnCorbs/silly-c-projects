#include <stdio.h>

int main()
{
	int num;
	num = 15;

	float num2;
	num2 = 1.23;

	char character;
	character = 'a';
	
	float sum;

	printf("Program to print out variables !!");
	printf("\nhere is a number: %d", num);
	printf("\nhere is a number as a decimal: %f", num2);
	printf("\nhere is a single character: %c", character);
	
	sum = num + num2;
	printf("\nhere is the sum of the two numbers above: %f\n", sum);

	return 0;

}
